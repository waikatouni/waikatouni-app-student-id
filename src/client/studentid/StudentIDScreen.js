/* eslint-disable no-console */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react/void-dom-elements-no-children */
import React, { useState, useEffect } from "react";
import _ from "lodash";
import moment from "moment";
import { Page, Container, ErrorMessage, request } from "@ombiel/aek-lib";

import PulseLoader from "react-spinners/PulseLoader";

var Barcode = require("react-barcode");
var graduation = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFoAAABaCAYAAAA4qEECAAAABmJLR0QA/wD/AP+gvaeTAAAG2klEQVR4nO2dfYwdVRmHnxcotHxYEfkyKd+NhFQwRqPRGLWgKagYTUkUP0LVxibVQAyJMYLEqAl+JKABI8HEEAXUGGKUEMWvkFQJili33YJQwJJaipaGBrRAu/v4x7nrbrd7751758zM3WWeZJPNzsz7/va3556ZOec9Z6GlpaWlpaWlpaWlpVLUk9VlajStZb5RyDD1FcDtwLs6P3oOGAfGgE3AZmAsIp6uQuRCoKjRPwQ+XODUJ0nGj5HM3wRsiYjnh1a4QChq9DPA0iFzTABbOdD8MeDxiJgcMua8o6jR24BTMuf+D7CFg7uff2fOMxIUNfpy4PqKtUzxFMn4/5tP6n7+W1P+Sij89KB+GbiqQi39eBL4C+kmvKXz/UMRMdGgpsIM9JimfgX4QkVahmEvyfiZrX9jROxqVFUO1KsdbSbUH6nHNO1VadQrGzazCPebnv/nN+o6dbJhM/vxgPrKpr0qjbrW9FEdZcbVk5v2qvSYhXopcAtwWHk5lfEQcH5E7GhKQJbBIfWDwA8YbbMfJpm9vYnk2Ubh1EuAW4FFuWJWwDZgZUQ8VnfirMOd6ruBnwKLc8bNzBOklr21zqTZx5XVVcAdwJLcsTOyE7ggIsbrSljJAL76NuBO4Ogq4mfiXySzN9WR7JAqgkbEPcBFwLNVxM/ECcA96hvqSFbplFTnl/glMMpvaM8AqyLiviqTVD73p74OuBs4rupcJdgDXBgR91aVoJKuYyYR8QBwATDKA/pLgV+rK6tKkM1o9c3djkXERmAlaVB/VDkK+Ll6ftNCuqJeqO5XP9rnvFer2+se7BiQ59WL6/JuINT7OiL3q5f1Ofc09bGmXCzIC+r7a7KvGOr7ZomcVNf3ueYUdWvd7g3IPrVIiUX1qKH+dQ6Rk6YJ3V7XLlMfrtG4YdivfqwuP3uZtbqHyEn1s32uP1HdVIdjJdivrqnL07lMOkT9WwGhX+wT54SCcZpkUv10Xd7ONujSAYRe2yfWsU7fUEeVSfWKuvydMuZQ9cEBhX69T8yXq/fmdqcC6qttUS8bUuSN9ij5VY9Wf5fNkuq4pg6TF6mPlhB5k9r1jVQ9Uv1NDjcqpmd3mMPotRlEfs/+Zv8qQ56q+UZVJh+hbssk8hb10B65FqvfUfdmylcV11tgBcSgtXfrgRsGuaYPPwY+EhH7e+Q8EjgHOBdYAbym83ViRh1l+S6wvle99yDVpIuBR4FXZRA2kzuAD0XEi4NcpB7PtPkrgPNIf5CjMusryveBT3YzexCjrwCuy6VqFncCqyPihTJBTP3+6Rzc+s+inpqTqyLiq3MdGMTodwI3AstzqZrF3SSzs88zdj6N53Bg619B/k/n9ohYNteBQfvoI4Crgc9RTQsZB9ZExJ8riH0QpmrT2a1/BTBsye+uiDg+k7w06Wp1w5wT6s/UD6i1zzOaRiRPV9+rft5Ua71ZfbGA9pu6xR16clZdCtwMXDJsjII8wfRqrqnK/gcHvXmWRT0c6HYPmQBuA9Z1W2uTo5r0SuBr1DDRO4N9wN+ZXk6xGdgUEf+oMqlql0OL+93Ic1WTriI9E78sR7wSPEuqGt3C9KKiP0VElknhbkZHRN4Xlj4izgPuIv+dPAf/ZLrrmep+xod4dm/e6I6Q04DfAmfkjFsBe4GzBi1ML2N01n6100e+A3gkZ9wKuKHu6v+qqklPAn4PnF1F/JI8B5wxzFLokWnRMxLvJJWBbasifkm+1cR686qrSZcDG0glsqPAHlJr3j3MxSPXomcIeAR4D+njOgpcN6zJZallyx71IuAX1PtSM5vdpNa8Z9gAI9uiZwi5C+hZ31ED3yxjcllq24TKNN3zE2B1XTlnsIvUmksNwY58iwaICIE1pNfiurm2inHuQah9WzX1bNJMzWLSuO8xne+XMvy+Tb3YCZyZYwebkXkFL4u6BDgVWEbaw+lU0szIa0mv9cPovTwivp1J38IwuhemjU7OBd4EvB14K/0/AduB5bm2gytj9LzFVP/3evVL6sYuMx7rMuZb1GNmZZTXv+dFPVO9xulytcc7syI5Yp+k/qGH0Rs64zsvHUx12xebNgDIEe9wi1W5/jHXH/YlifqpAiZPsbZpvfOWTkstyoZucRbu3TIT6m7g2IKn746IOUskmhzkmS/sy3Fua3R/Bqmaur/bgdbo/tw2wLm3VqZioaMuUXcUuBHuMA0htAyL+pkCRjezDnEhYXpp6bVRwDZTpW1LWdRP9DD6403rWzCoh6ljc5g8po7yDpbzD/WNHrjp7aT6lqZ1LUhMaySnuLlpPQsW9Tj1qc5X4RUJbd8yIBHx9NSjXPuflFpaWlpaWlpGj/8BymTWsYTPxGkAAAAASUVORK5CYII=";

const StudentIDScreen = () => {
  const [studentID, setstudentID] = useState(
    JSON.parse(localStorage.getItem("studentIDValue")) || []
  );
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");

  const name = studentID.name;

  // The constant below is coming from the master.ect file
  const studentEmail = email;

  useEffect(() => {
    if (_.isEmpty(studentID)) {
      request
        .action("get-id")
        .end((err, res) => {
          if (res.body) {
            localStorage.setItem("studentIDValue", JSON.stringify(res.body));
            setstudentID(res.body);
            setLoading(false);
          } else {
            setError(`We couldn't find the student ID for user: ${studentEmail}. Please try again later or report this issue to campusm@waikato.ac.nz \n\n ${res.text}`);
            setLoading(false);
          }
        });
    } else {
      setLoading(true);
      request
        .action("get-id")
        .end((err, res) => {
          if(res.body) {
            localStorage.setItem("studentIDValue", JSON.stringify(res.body));
            setstudentID(res.body);
            setLoading(false);
          }
          else {
            setError(`We couldn't find the student ID for user: ${studentEmail}. Please try again later or report this issue to campusm@waikato.ac.nz \n\n ${res.text}`);
            setLoading(false);
          }
        });
    }
  }, [setstudentID]);

  return (
    <Page>
      <Container>
        <div
          style={{
            width: "100%",
            height: "100%",
            justifyContent: "center",
            display: "flex",
            backgroundColor: "white",
          }}
        >
          <div style={{ maxWidth: "450px", width: "100%", backgroundColor: "#be0403", borderRadius: 20, height: "max-content", overflow: "hidden", borderStyle: "solid", borderWidth: 2, borderColor: "#be0403" }}>
          {error && (
            <ErrorMessage heading="Sorry Something Sent Wrong">
              {error}
            </ErrorMessage>
          )}
          {loading && (
            <div
              style={{
                width: "100%",
                height: "100%",
                justifyContent: "center",
                alignItems: "center",
                display: "flex",
              }}
            >
              <PulseLoader size={15} loading={loading} color="white" />
            </div>
          )}
          {!loading && !error && (
            <div
              style={{
                width: "100%",
                height: "110px",
                display: "flex",
                justifyContent: "center",
                padding: 20,
              }}
            >
              <img
                src="https://squiz-matrix.its.waikato.ac.nz/__matrix-data/assets/file/0015/33225/CoA-transparent.svg"
                style={{ maxWidth: "320px" }}
                alt="The University of Waikato - Te Whare Wānanga o Waikato"
              ></img>
            </div>
          )}
          {!loading && !error && (
            <h1
              style={{
                textAlign: "center",
                color: "white",
                marginTop: 0,
                fontSize: 21,
                paddingBottom: 10,
              }}
            >
              STUDENT ID CARD
            </h1>
          )}
          {!loading && !error && (
            <div
              style={{
                display: "flex",
                alignItems: "center",
              }}
            >
              <div style={{ width: "50%", position: "relative" }}>
                {studentID.isGraduated && <img src={graduation} style={{ position: "absolute", right: -20, top: -20, width: 80 }} />}
                <img src={`${studentID.picture}`} style={{ maxWidth: "100%" }} />
              </div>
              <div style={{ width: "50%" }}>
                <h2
                  style={{
                    color: "white",
                    textAlign: "center",
                    fontSize: 18,
                    marginBottom: 5,
                  }}
                >
                  ID Number
                </h2>
                <h1
                  style={{
                    marginTop: 0,
                    color: "white",
                    fontSize: 30,
                    textAlign: "center",
                  }}
                >
                  {studentID.realStudentId}
                </h1>
                <h3 style={{ color: "white", textAlign: "center", paddingTop: 0, fontSize: 12, textAlign: "center", width: "100%" }}>
                  Issued:{" "}{moment(studentID.issued).format("Do MMM YYYY")}
                </h3>
                <h3 style={{ color: "white", textAlign: "center", paddingTop: 0, marginTop: 10, fontSize: 12, textAlign: "center", width: "100%" }}>
                  Valid Until:{" "}{moment(studentID.validuntil).format("Do MMM YYYY")}
                </h3>
              </div>
            </div>
          )}
          {!loading && !error && (
            <h1
              style={{
                marginTop: 10,
                marginBottom: 20,
                marginLeft: 20,
                color: "white",
                fontSize: "1.8rem",
                textAlign: "left",
              }}
            >
              {name}
            </h1>
          )}
          {!loading && !error && (
            <div
              style={{
                display: "flex",
                width: "100%",
                justifyContent: "center",
                backgroundColor: "white",
                padding: 10,
              }}
            >
              <Barcode value={studentID.studentID} format="CODE39" width={2} margin={5} />
            </div>
          )}
          </div>
        </div>
      </Container>
    </Page>
  );
};

export default StudentIDScreen;
