import React from "react";
import {reactRender} from "@ombiel/aek-lib";

import StudentIDScreen from "./studentid/StudentIDScreen";

reactRender(<StudentIDScreen />);
